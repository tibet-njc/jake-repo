# jake-repo

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Git/Bitbucket workflow
1. From the root directory, checkout `main` branch.
   ```
   git checkout main
   ```
2. Pull latest code from bitbucket.
     ```
     git pull
     ```
3. Create and checkout new feature branch.
    ```
    git checkout -b <your-feature-branch>
    ```
4. You should now be in the feature branch. Add changes to your code in your editor. After any significant change, stage and commit the code.
    ```
    git add <path-to-changed-file> <path-to-other changed-file>
    ```
    or:
    ```
    git add <path-to-directory-with-multiple-changed-files>
    ```
    or to stage all changed files (the easy way):
    ```
    git add --all
    ```
    _**NOTE:** At any time you can run `git status` to see current changed files, staged files and unstaged files. There may be times when you don't want to stage all changed files, but most of the time you do._

5. Commit your changes with a meaningful commit message explaining the purpose fo the commit. Think why, not what. A common best practice is to use the present tense and have your message complete the sentence "This commmit will..."
    ```
    git commit -m 'provide Nick with git workflow documentation'
    ```
6. Push your branch to bitbucket and create remote branch
    ```
    git push -u origin <your-feature-branch>
    ```
    if your remote branch already exists in bitbucket you can simply use the command:
    ```
    git push
    ```
7. Continue to add/remove code until the feature is complete. Follow steps 4-6 to stage, commit and push each commit to the repo.
8. In bit bucket navigate to your feature branch and create a pull request from `your-feature-branch` to `main`.  Add a description of the feature and add reviewers. Additionally you can add comments to the individual lines of the changed code itself by looking at the diff and clicking on the line where you want to add a comment. Note this is just a comment for the PR, not an actual code comment in the source code. 
9. Reviewers may offer feedback via comments. If work is still needed follow steps 4-6 to make more changes. The existing PR will automatically be updated with the new commits. 
10. When the code is approved, click the button to merge the PR. the `main` branch will be updated with all the changes from `your-feature-branch` In the case that there are merge conflicts bitbucket will inform you that you need to manually resolve them and provide instructions (I think). After a succesful merge start the whole process over at step 1 to add your next feature.
### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
